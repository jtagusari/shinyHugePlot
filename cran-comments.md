## Resubmission
This is a resubmission (v0.2.6). 

In this version I have:
* fixed a bug, where data with a lot of NAs throws an error.
* explicitly give arguments to `dplyr::across`

## Resubmission
This is a resubmission (v0.2.5). 

In this version I have:
* fixed a bug, where `rng_stat_aggregator` will not work.


## Resubmission
This is a resubmission (v0.2.4). 

In this version I have:
* fixed a bug, where traces are duplicated if the zoom level was too close and no data were found.
* updated help documents: added "_PACKAGE" in `shinyHugePlot.R`.


### Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.2
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.2

### R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

### Downstream dependencies
There are no downstream dependencies.

## Resubmission
This is a resubmission (v0.2.3). 

In this version I have:
* fixed bugs on plotly_build_light.
* updated help documents.


### Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.2
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.2

### R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

### Downstream dependencies
There are no downstream dependencies.

## Resubmission
This is a resubmission (v0.2.2). 

In this version I have:
* fixed bugs on LTTB_aggregator.
* updated help documents.

### Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.2
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.2

### R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

### Downstream dependencies
There are no downstream dependencies.


## Past comments

### Resubmission
This is a resubmission (v0.2.1). 

In this version I have:
* changed methods for handling data (tibble to data.table) inside the package for shorten computational time.
* replaced sample data with a new one.
* fixed several bugs.

#### Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.2
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.2

#### R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

#### Downstream dependencies
There are no downstream dependencies.

### Resubmission
This is a resubmission (v0.2.0). 

In this version I have:
* thoroughly changed list-type data to tibble-type data.
* replaced abstract_downsampler with plotly_datahandler and downsampler.
* registered utility functions in plotly_datahandler as methods. 
* integrated shiny_downsampler to shiny_hugeplot.
* changed the name of abstract_aggretator to aggregator.
* added an R6 class of rng_aggregator, which is a super class for range_stat_aggregator.
* added plotly_build_light function
* fixed several bugs, mainly about handling nanotime data.

#### Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.1
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.1

#### R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

#### Downstream dependencies
There are no downstream dependencies.


### Resubmission
This is a resubmission (v 0.1.0). 

In this version I have:
* add a new aggregator (null_aggregator).
* update shiny_downsampler$show_shiny to interactively update the aggregator and sample size.
* update abstract_downsampler to plot data that contains name column.

#### Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.1
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.1

#### R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

#### Downstream dependencies
There are no downstream dependencies.


### Resubmission
This is a resubmission (v 0.0.1). 

In this version I have:
* Wrote package names in single quotes in title and descriptions.
* Added a reference that describes the down-sampling methods employed in the package.
* Changed the "updatePlotlyH" function to be exported.
* Replaced \dontrun with \donttest.

#### Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.0
─ local x86_64-w64-mingw32 (64-bit), R 4.2.1
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.1

#### R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

#### Downstream dependencies
There are no downstream dependencies.
